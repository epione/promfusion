# ProMFusion

Code related to the paper:

*Robust Fusion of Probability Maps*

by Benoît Audelan, Dimitri Hamzaoui, Sarah Montagne, Raphaële Renard-Penna and Hervé Delingette. 

Link: https://hal.inria.fr/hal-02934590

# Installation

## GNU/Linux
Install conda:
```bash
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh  
bash Miniconda3-latest-Linux-x86_64.sh
```

Download this github repository and move into in:
```bash
git clone git://gitlab.inria.fr/baudelan/promfusion
cd promfusion
```

Install the customized python environment:
```bash
conda env create -f environment.yml
```

Activate the python environment:
```bash
conda activate promfusion
```

Install the ProMFusion package:
```bash
python setup.py install
```

An alternative to the last point is to install the package in "develop" mode.
Using this mode, all local modifications of source code will be considered in your Python interpreter (when restarted) without having to install the package again.
This is particularly useful when adding new features.
To install this package in develop mode, type the following command line:
```bash
python setup.py develop
```

## Windows
Download and install conda from: https://docs.conda.io/en/latest/miniconda.html

Download this github repository from: https://gitlab.inria.fr/baudelan/promfusion

Open the Anaconda prompt and move into the github repository previously downloaded.

Deactivate the base environment:
`conda deactivate`

Install the customized python environment:
`conda env create -f environment.yml`

Activate the python environment:
`conda activate promfusion`

Install the ProMFusion package:
`python setup.py install`

An alternative to the last point is to install the package in "develop" mode.
Using this mode, all local modifications of source code will be considered in your Python interpreter (when restarted) without having to install the package again.
This is particularly useful when adding new features.
To install this package in develop mode, type the following command line:
`python setup.py develop`

# Usage

We give a use case example for the fusion of segmentations with a lung nodule taken from the LIDC dataset (https://doi.org/10.1118/1.3528204). The algorithm's settings are defined and presented in the config.py file. Two models can be used, a non robust approach using a Gaussian likelihood or the proposed robust approach using a Student's *t* likelihood. The model can be fitted with or without spatial regularization. 

# Licence

Copyright (C) 2020  Benoît Audelan

This file is part of ProMFusion.

ProMFusion is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ProMFusion is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ProMFusion.  If not, see <https://www.gnu.org/licenses/>.
