#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import torch
import scipy.sparse


def multivariate_normal_pdf_2D(scale, dtype):

    ii = np.arange(-scale - 1, scale + 2, 1)
    jj = np.arange(-scale - 1, scale + 2, 1)

    ni, nj = len(ii), len(jj)

    ii, jj = np.meshgrid(ii, jj, indexing='ij')

    ii = np.reshape(ii, (-1, 1))
    jj = np.reshape(jj, (-1, 1))

    var = (scale / 3) ** 2
    res = (ii ** 2 + jj ** 2).astype(np.float64)
    ind_0 = np.where(res > scale ** 2)  # Set to 0 if > to 3 standard deviation
    ind_1 = np.where(res <= scale ** 2)
    res[ind_0] = 0
    res[ind_1] = 1 / (2 * np.pi) * 1 / var * np.exp(-1 / 2 * 1 / var * res[ind_1])

    res = np.reshape(res, (ni, nj)).astype(dtype)

    return res

def multivariate_normal_pdf_3D(scale, dtype):

    zz = np.arange(-scale - 1, scale + 2, 1)
    ii = np.arange(-scale - 1, scale + 2, 1)
    jj = np.arange(-scale - 1, scale + 2, 1)

    nz, ni, nj = len(zz), len(ii), len(jj)

    zz, ii, jj = np.meshgrid(zz, ii, jj, indexing='ij')

    zz = np.reshape(zz, (-1, 1))
    ii = np.reshape(ii, (-1, 1))
    jj = np.reshape(jj, (-1, 1))

    var = (scale / 3) ** 2
    res = (zz ** 2 + ii ** 2 + jj ** 2).astype(np.float64)
    ind_0 = np.where(res > scale ** 2)  # Set to 0 if > to 3 standard deviation
    ind_1 = np.where(res <= scale ** 2)
    res[ind_0] = 0
    res[ind_1] = 1 / (2 * np.pi) * 1 / (var**3)**0.5 * np.exp(-1 / 2 * 1 / var * res[ind_1])

    res = np.reshape(res, (nz, ni, nj)).astype(dtype)

    return res

class Gaussian_FPM_GLSP():

    def __init__(self, config):

        self.dim = config["dim"]
        self.n_iter = config["n_iter"]
        self.config = config
        self.tol = config["tol"]
        self.eps = config["eps"]
        self.dtype_pytorch = config["torch_dtype"]
        self.dtype_numpy = config["numpy_dtype"]
        self.spatial_prior = self.config["spatial_prior"]
        if self.config["use_gpu"]:
            self.device = torch.device("cuda:0")
        else:
            self.device = torch.device("cpu")

    def initialization(self, D):
        '''
        :param D: N x P x K
        :return:
        '''

        mu_b = torch.zeros((self.P, self.K), dtype=self.dtype_pytorch, device=self.device)
        precision_p = torch.stack([torch.eye(self.K, dtype=self.dtype_pytorch, device=self.device)*1e2 for i in range(self.P)])
        beta = np.random.random()
        alpha = np.random.random()
        precision_T = 1 / (torch.mean(torch.var(D, dim=1)) + self.eps)
        mu_Tn = torch.mean(D, dim=1)

        return mu_b, precision_p, beta, alpha, precision_T, mu_Tn

    def createBasisFunctions_2D(self, narrow_band):

        flatten_indices = np.arange(self.N_tot)
        flatten_indices = np.reshape(flatten_indices, self.img_shape)

        basis_functions = []
        icoord = []
        jcoord = []

        scale = self.config["scale"]
        step = self.config["step"]
        layout = self.config["layout"]
        assert (layout == "square" or layout == "quinconce")

        ii_basis_centers = np.arange(self.img_shape[0])
        jj_basis_centers = np.arange(self.img_shape[1])
        ii_basis_centers = ii_basis_centers % step
        ii_basis_centers = np.where(ii_basis_centers == 0)[0]
        jj_basis_centers = jj_basis_centers % step
        jj_basis_centers = np.where(jj_basis_centers == 0)[0]

        if layout == "quinconce":
            ii_basis_centers = ii_basis_centers[0::2]
            jj_basis_centers = jj_basis_centers[0::2]

        ii_basis_centers, jj_basis_centers = np.meshgrid(ii_basis_centers, jj_basis_centers, indexing="ij")
        ii_basis_centers, jj_basis_centers = np.reshape(ii_basis_centers, (-1, 1)), np.reshape(jj_basis_centers,
                                                                                               (-1, 1))

        if layout == "quinconce":
            ii_sup = (ii_basis_centers + step).astype(np.int)
            jj_sup = (jj_basis_centers + step).astype(np.int)
            ind_to_remove = np.logical_or(ii_sup >= self.img_shape[0], jj_sup >= self.img_shape[1])
            ind_to_remove = np.arange(len(ii_sup))[np.reshape(ind_to_remove, (-1,))]
            ii_sup = np.reshape(np.delete(ii_sup, ind_to_remove), (-1, 1))
            jj_sup = np.reshape(np.delete(jj_sup, ind_to_remove), (-1, 1))
            ii_basis_centers = np.concatenate([ii_basis_centers, ii_sup])
            jj_basis_centers = np.concatenate([jj_basis_centers, jj_sup])

        box = multivariate_normal_pdf_2D(scale, self.dtype_numpy)
        ni_box, nj_box = box.shape
        ni_box = int((ni_box - 1) / 2)
        nj_box = int((nj_box - 1) / 2)
        ni, nj = self.img_shape

        # We compute the values of all pixels for each basis function
        for j in range(len(ii_basis_centers)):
            i_bf = int(ii_basis_centers[j])
            j_bf = int(jj_basis_centers[j])

            # If basis function belongs to the narrow band
            if narrow_band[i_bf, j_bf]:

                istart = max(0, i_bf - ni_box)
                iend = min(istart + (i_bf - istart) + ni_box + 1, ni)

                ibox_start = max(0, ni_box - i_bf)
                ibox_end = ibox_start + (iend - istart)

                jstart = max(0, j_bf - nj_box)
                jend = min(jstart + (j_bf - jstart) + nj_box + 1, nj)

                jbox_start = max(0, nj_box - j_bf)
                jbox_end = jbox_start + (jend - jstart)

                values = box[ibox_start:ibox_end, jbox_start:jbox_end]
                flatten_ind = flatten_indices[istart:iend, jstart:jend]

                non_zeros = np.where(values > 0)
                values = values[non_zeros]
                flatten_ind = flatten_ind[non_zeros]

                bf_values = scipy.sparse.coo_matrix((values, (flatten_ind, np.zeros(flatten_ind.shape))), shape=[self.N_tot, 1])

                basis_functions.append(bf_values)
                icoord.append(i_bf)
                jcoord.append(j_bf)

        basis_functions = scipy.sparse.hstack(basis_functions)

        return basis_functions, icoord, jcoord

    def createBasisFunctions_3D(self, narrow_band):

        flatten_indices = np.arange(self.N_tot)
        flatten_indices = np.reshape(flatten_indices, self.img_shape)

        basis_functions = []
        zcoord = []
        icoord = []
        jcoord = []

        scale = self.config["scale"]
        step = self.config["step"]
        layout = self.config["layout"]
        assert (layout == "square" or layout == "quinconce")

        zz_basis_centers = np.arange(self.img_shape[0])
        ii_basis_centers = np.arange(self.img_shape[1])
        jj_basis_centers = np.arange(self.img_shape[2])
        zz_basis_centers = zz_basis_centers % step
        zz_basis_centers = np.where(zz_basis_centers == 0)[0]
        ii_basis_centers = ii_basis_centers % step
        ii_basis_centers = np.where(ii_basis_centers == 0)[0]
        jj_basis_centers = jj_basis_centers % step
        jj_basis_centers = np.where(jj_basis_centers == 0)[0]

        if layout == "quinconce":
            zz_basis_centers = zz_basis_centers[0::2]
            ii_basis_centers = ii_basis_centers[0::2]
            jj_basis_centers = jj_basis_centers[0::2]

        zz_basis_centers, ii_basis_centers, jj_basis_centers = np.meshgrid(zz_basis_centers, ii_basis_centers,
                                                                           jj_basis_centers, indexing="ij")
        zz_basis_centers, ii_basis_centers, jj_basis_centers = np.reshape(zz_basis_centers, (-1, 1)), np.reshape(
            ii_basis_centers, (-1, 1)), np.reshape(jj_basis_centers, (-1, 1))

        if layout == "quinconce":
            zz_sup = (zz_basis_centers + step).astype(np.int)
            ii_sup = (ii_basis_centers + step).astype(np.int)
            jj_sup = (jj_basis_centers + step).astype(np.int)
            ind_to_remove = np.logical_or(ii_sup >= self.img_shape[1], jj_sup >= self.img_shape[2])
            ind_to_remove = np.logical_or(ind_to_remove, zz_sup >= self.img_shape[0])
            ind_to_remove = np.arange(len(ii_sup))[np.reshape(ind_to_remove, (-1,))]
            zz_sup = np.reshape(np.delete(zz_sup, ind_to_remove), (-1, 1))
            ii_sup = np.reshape(np.delete(ii_sup, ind_to_remove), (-1, 1))
            jj_sup = np.reshape(np.delete(jj_sup, ind_to_remove), (-1, 1))
            zz_basis_centers = np.concatenate([zz_basis_centers, zz_sup])
            ii_basis_centers = np.concatenate([ii_basis_centers, ii_sup])
            jj_basis_centers = np.concatenate([jj_basis_centers, jj_sup])

        box = multivariate_normal_pdf_3D(scale, self.dtype_numpy)
        nz_box, ni_box, nj_box = box.shape
        nz_box = int((nz_box - 1) / 2)
        ni_box = int((ni_box - 1) / 2)
        nj_box = int((nj_box - 1) / 2)
        nz, ni, nj = self.img_shape

        # We compute the values of all pixels for each basis function
        for j in range(len(ii_basis_centers)):
            z_bf = int(zz_basis_centers[j])
            i_bf = int(ii_basis_centers[j])
            j_bf = int(jj_basis_centers[j])

            # If basis function belongs to the narrow band
            if narrow_band[z_bf, i_bf, j_bf]:

                zstart = max(0, z_bf - nz_box)
                zend = min(zstart + (z_bf - zstart) + nz_box + 1, nz)

                zbox_start = max(0, nz_box - z_bf)
                zbox_end = zbox_start + (zend - zstart)

                istart = max(0, i_bf - ni_box)
                iend = min(istart + (i_bf - istart) + ni_box + 1, ni)

                ibox_start = max(0, ni_box - i_bf)
                ibox_end = ibox_start + (iend - istart)

                jstart = max(0, j_bf - nj_box)
                jend = min(jstart + (j_bf - jstart) + nj_box + 1, nj)

                jbox_start = max(0, nj_box - j_bf)
                jbox_end = jbox_start + (jend - jstart)

                flatten_ind = flatten_indices[zstart:zend, istart:iend, jstart:jend]
                values = box[zbox_start:zbox_end, ibox_start:ibox_end, jbox_start:jbox_end]

                non_zeros = np.where(values > 0)
                flatten_ind = flatten_ind[non_zeros]
                values = values[non_zeros]

                bf_values = scipy.sparse.coo_matrix((values, (flatten_ind, np.zeros(flatten_ind.shape))), shape=[self.N_tot, 1])

                basis_functions.append(bf_values)
                zcoord.append(z_bf)
                icoord.append(i_bf)
                jcoord.append(j_bf)

        basis_functions = scipy.sparse.hstack(basis_functions)

        return basis_functions, zcoord, icoord, jcoord

    def q_Tn_with_prior(self, precision_p, D, mu_b, basis_functions, mu_w, precision_T):
        '''
        :param precision_p: P x K x K
        :param D: N x P x K
        :param mu_b: P x K
        :param basis_functions: N x L
        :param mu_w: L x K
        :param precision_T: scalar
        :return: sigma_Tn, mu_Tn
        '''

        sigma_Tn = torch.sum(precision_p, dim=0) + torch.eye(self.K, dtype=self.dtype_pytorch, device=self.device) * precision_T
        sigma_Tn = torch.inverse(sigma_Tn) # K x K
        sigma_Tn = torch.repeat_interleave(sigma_Tn[None, ...], self.N, dim=0) # N x K x K

        D_b = D - mu_b #  N x P x K
        precision_p_D_b = torch.einsum('pkl,npl->nk', precision_p, D_b) # N x K

        bf_mu_w = torch.sparse.mm(basis_functions, mu_w)  # N x K
        precision_T_bf_mu_w = precision_T * bf_mu_w # N x K

        mu_Tn = torch.einsum('nkl,nl->nk', sigma_Tn, precision_p_D_b+precision_T_bf_mu_w) # N x K

        return mu_Tn, sigma_Tn, bf_mu_w

    def q_Tn_without_prior(self, precision_p, D, mu_b):
        '''
        :param precision_p: P x K x K
        :param D: N x P x K
        :param mu_b: P x K
        :return: sigma_Tn, mu_Tn
        '''

        sigma_Tn = torch.sum(precision_p, dim=0)
        sigma_Tn = torch.inverse(sigma_Tn)
        sigma_Tn = torch.repeat_interleave(sigma_Tn[None, ...], self.N, dim=0)  # N x K x K

        D_b = D - mu_b #  N x P x K
        precision_p_D_b = torch.einsum('pkl,npl->nk', precision_p, D_b) # N x K

        mu_Tn = torch.einsum('nkl,nl->nk', sigma_Tn, precision_p_D_b) # N x K

        return mu_Tn, sigma_Tn


    def q_w(self, basis_functions, precision_T, alpha, mu_Tn, bf_bf):
        '''
        :param basis_functions: N x L
        :param precision_T: scalar
        :param alpha: scalar
        :param mu_Tn: N x K
        :param bf_bf: L x L
        :return: mu_w, sigma_w
        '''

        sigma_w = precision_T * bf_bf + torch.eye(self.L, dtype=self.dtype_pytorch, device=self.device) * alpha # L x L
        sigma_w = torch.inverse(sigma_w[np.newaxis, ...])[0] # L x L  // It is the same matrix for all K

        bf_mu_Tn = torch.sparse.mm(basis_functions.t(), mu_Tn) # L x K
        precision_T_bf_mu_Tn = precision_T * bf_mu_Tn # L x K
        mu_w = torch.einsum('lj,jk->lk', sigma_w, precision_T_bf_mu_Tn) # L x K

        return mu_w, sigma_w

    def q_alpha(self, sigma_w, mu_w):
        '''
        :param sigma_w: L x L
        :param mu_w: L x K
        :return: alpha
        '''

        alpha = self.L * self.K / (torch.sum(mu_w**2) + torch.trace(sigma_w) * self.K)

        return alpha

    def q_b(self, precision_p, beta, D, mu_Tn):
        '''
        :param precision_p: P x K x K
        :param beta: scalar
        :param D: N x P x K
        :param mu_Tn: N x K
        :return: mu_b, sigma_b
        '''

        sigma_b = self.N * precision_p + beta*torch.eye(self.K, dtype=self.dtype_pytorch, device=self.device) # P x K x K
        sigma_b = torch.inverse(sigma_b)

        D_Tn = D.permute(1,0,2) - mu_Tn # P x N x K
        D_Tn = D_Tn.permute(1,0,2) # N x P x K
        D_Tn = torch.sum(D_Tn, dim=0) # P x K
        precision_p_D_Tn = torch.einsum('pkl,pl->pk', precision_p, D_Tn) # P x K
        mu_b = torch.einsum('pkl,pl->pk', sigma_b, precision_p_D_Tn)  # P x K

        # Values that will be used later
        D_Tn_mu_b = D.permute(1,0,2) - mu_Tn # P x N x K
        D_Tn_mu_b = D_Tn_mu_b.permute(1,0,2) # N x P x K
        D_Tn_mu_b = D_Tn_mu_b - mu_b

        return mu_b, sigma_b, D_Tn_mu_b

    def q_beta(self, sigma_b, mu_b):
        '''
        :param sigma_b: P x K x K
        :param mu_b: P x K
        :return: beta
        '''

        beta = self.P * self.K / torch.sum(torch.einsum('pkk', sigma_b) + torch.sum(mu_b**2, dim=1))

        return beta

    def q_precision_p(self, sigma_Tn, sigma_b, D_mu_Tn_mu_b):
        '''
        :param sigma_Tn: N x K x K
        :param sigma_b: P x K x K
        :param D_mu_Tn_mu_b: N x P x K
        :return: precision_p, sigma_p
        '''

        m = torch.einsum('npk,npl->pkl', D_mu_Tn_mu_b, D_mu_Tn_mu_b)  # P x K x K
        sigma_Tn = torch.sum(sigma_Tn, dim=0) # K x K
        sigma_b = self.N * sigma_b  # P x K x K
        sigma_p = (m + sigma_Tn + sigma_b) / self.N + torch.eye(self.K, dtype=self.dtype_pytorch, device=self.device) * self.eps # P x K x K

        precision_p = torch.inverse(sigma_p)

        return precision_p, sigma_p

    def q_precision_T(self, mu_Tn, bf_mu_w, bf_bf, sigma_Tn, sigma_w):
        '''
        :param mu_Tn: N x K
        :param bf_mu_w: N x K
        :param bf_bf: L x L
        :param sigma_Tn: N x K x K
        :param sigma_w: L x L
        :return: precision_T, sigma_T
        '''

        mu_Tn_bf_mu_w = mu_Tn - bf_mu_w # N x K
        maha = mu_Tn_bf_mu_w ** 2 # N x K

        trace_bf_bf_sigma_w = torch.trace(torch.einsum('lj,jk->lk', bf_bf, sigma_w)) * self.K  # L x L  // It is the same for all K

        trace_sigma_Tn = torch.sum(torch.einsum('nkk', sigma_Tn))

        sigma_T = 1/(self.N * self.K) * (torch.sum(maha) + trace_sigma_Tn + trace_bf_bf_sigma_w) + self.eps

        precision_T = 1 / sigma_T

        return precision_T, sigma_T

    def lower_bound_with_prior(self, precision_p, precision_T, mu_Tn, mu_b, mu_w, sigma_Tn, sigma_b,
                               sigma_w, beta, alpha, bf_bf, bf_mu_w, D_mu_Tn_mu_b,
                               traces_precision_p_sigma_b_sigma_Tn):
        '''
        :param precision_p: P x K x K
        :param precision_T: scalar
        :param mu_Tn: N x K
        :param mu_b: P x K
        :param mu_w: L x K
        :param sigma_Tn: N x K x K
        :param sigma_b: P x K x K
        :param sigma_w: L x L
        :param beta: scalar
        :param alpha: scalar
        :param bf_bf: L x L
        :param bf_mu_w: N x K
        :param D_mu_Tn_mu_b: N x P x K
        :param traces_precision_p_sigma_b_sigma_Tn: N x P
        :return: lower bound
        '''

        _, log_det_precision_p = torch.slogdet(precision_p)
        _, log_det_sigma_Tn = torch.slogdet(sigma_Tn)
        _, log_det_sigma_b = torch.slogdet(sigma_b)
        _, log_det_sigma_w = torch.slogdet(sigma_w)

        maha = torch.einsum('npk,pkl,npl->np', D_mu_Tn_mu_b, precision_p, D_mu_Tn_mu_b)  # N x P

        traces = traces_precision_p_sigma_b_sigma_Tn # N x P

        p_D = 1/2 * log_det_precision_p - 1/2 * maha - 1/2 * traces # N x P
        p_D = torch.sum(p_D)

        mu_Tn_bf_mu_w = mu_Tn - bf_mu_w # N x K
        precision_T_diag = torch.eye(self.K, dtype=self.dtype_pytorch, device=self.device) * precision_T # K x K
        maha = torch.einsum('nk,kl,nl->n', mu_Tn_bf_mu_w, precision_T_diag, mu_Tn_bf_mu_w) # N
        precision_T_diag_sigma_Tn = torch.einsum('kl,nlj->nkj', precision_T_diag, sigma_Tn) # N x K x K
        precision_T_bf_bf_sigma_w = torch.einsum('lj,jk->lk', precision_T * bf_bf, sigma_w) # L x L
        trace = torch.einsum('nkk', precision_T_diag_sigma_Tn) # N

        p_Tn = self.K/2 * torch.log(precision_T) - 1/2 * maha - 1/2 * trace # N
        p_Tn = torch.sum(p_Tn) - 1/2 * torch.trace(precision_T_bf_bf_sigma_w) * self.K

        p_w = self.L/2 * torch.log(alpha) - alpha/2 * (torch.trace(sigma_w) + torch.sum(mu_w**2, dim=0)) # K
        p_w = torch.sum(p_w)

        p_b = self.K/2 * torch.log(beta) - 1/2 * beta * (torch.sum(mu_b**2, dim=1) + torch.einsum('pkk', sigma_b)) # P
        p_b = torch.sum(p_b)

        q_Tn = - 1/2 * log_det_sigma_Tn  # N
        q_Tn = torch.sum(q_Tn)

        q_b = -1/2 * log_det_sigma_b  # P
        q_b = torch.sum(q_b)

        q_w =  -self.K/2 * log_det_sigma_w

        lb = p_D + p_b + p_Tn + p_w - q_b - q_Tn - q_w

        return lb

    def lower_bound_without_prior(self, precision_p, mu_b, sigma_Tn, sigma_b, beta, D_mu_Tn_mu_b,
                                  traces_precision_p_sigma_b_sigma_Tn):
        '''
        :param precision_p: P x K x K
        :param mu_b: P x K
        :param sigma_Tn: N x K x K
        :param sigma_b: P x K x K
        :param beta: scalar
        :param D_mu_Tn_mu_b: N x P x K
        :param traces_precision_p_sigma_b_sigma_Tn: N x P
        :return: lower bound
        '''

        _, log_det_precision_p = torch.slogdet(precision_p)
        _, log_det_sigma_Tn = torch.slogdet(sigma_Tn)
        _, log_det_sigma_b = torch.slogdet(sigma_b)

        maha = torch.einsum('npk,pkl,npl->np', D_mu_Tn_mu_b, precision_p, D_mu_Tn_mu_b)  # N x P

        traces = traces_precision_p_sigma_b_sigma_Tn # N x P

        p_D = 1/2 * log_det_precision_p - 1/2 * maha - 1/2 * traces # N x P
        p_D = torch.sum(p_D)

        p_b = self.K/2 * torch.log(beta) - 1/2 * beta * (torch.sum(mu_b**2, dim=1) + torch.einsum('pkk', sigma_b)) # P
        p_b = torch.sum(p_b)

        q_Tn = - 1/2 * log_det_sigma_Tn  # N
        q_Tn = torch.sum(q_Tn)

        q_b = -1/2 * log_det_sigma_b  # P
        q_b = torch.sum(q_b)

        lb = p_D + p_b - q_b - q_Tn

        return lb

    def fit(self, D, narrow_band):
        '''
        :param D: N x P x K
        :param: narrow_band: img_shape
        '''

        D = D.astype(self.dtype_numpy)
        self.img_shape = narrow_band.shape
        self.N_tot = np.prod(self.img_shape)
        self.N, self.P, self.K = D.shape

        if self.spatial_prior:
            # Create the basis functions
            print("Starting building basis functions")
            if self.config["dim"] == 2:
                basis_functions, icoord, jcoord = self.createBasisFunctions_2D(narrow_band)
            else:
                basis_functions, zcoord, icoord, jcoord = self.createBasisFunctions_3D(narrow_band)
            print("End building basis functions")

            self.L = basis_functions.shape[1]  # Number of basis functions

            basis_functions = basis_functions.tocsr()
            basis_functions = basis_functions[np.reshape(narrow_band, (-1,)), :] # N x L
            bf_bf = torch.zeros((self.L, self.L), dtype=self.dtype_pytorch)
            tile = np.arange(0, self.N, 500000)
            for i in range(len(tile)):
                if i < len(tile) - 1:
                    bf = basis_functions[tile[i]:tile[i + 1], :].toarray()
                else:
                    bf = basis_functions[tile[i]:, :].toarray()
                bf = torch.from_numpy(bf)
                bf_bf += torch.einsum('nl,nk->lk', bf, bf)

        # Go to torch tensor
        D = torch.from_numpy(D).to(self.device)

        mu_b, precision_p, beta, alpha, precision_T, mu_Tn = self.initialization(D)

        # Values that are fixed
        if self.spatial_prior:
            basis_functions_cpu = basis_functions
            basis_functions = basis_functions.tocoo()
            index = np.concatenate([np.reshape(basis_functions.row, (1, -1)),
                                    np.reshape(basis_functions.col, (1, -1))], axis=0).astype(int)
            basis_functions = torch.sparse.FloatTensor(torch.from_numpy(index), torch.from_numpy(basis_functions.data),
                                                       torch.Size([self.N, self.L])).to(self.device)
            bf_bf = bf_bf.to(self.device)

        lower_bound_list = []
        converged = 0
        i = 0
        while i < self.n_iter:

            if self.spatial_prior:
                mu_w, sigma_w = self.q_w(basis_functions, precision_T, alpha, mu_Tn, bf_bf)
                alpha = self.q_alpha(sigma_w, mu_w)
                mu_Tn, sigma_Tn, bf_mu_w = self.q_Tn_with_prior(precision_p, D, mu_b, basis_functions, mu_w, precision_T)
                precision_T, sigma_T = self.q_precision_T(mu_Tn, bf_mu_w, bf_bf, sigma_Tn, sigma_w)
            else:
                mu_Tn, sigma_Tn = self.q_Tn_without_prior(precision_p, D, mu_b)

            mu_b, sigma_b, D_mu_Tn_mu_b = self.q_b(precision_p, beta, D, mu_Tn)

            beta = self.q_beta(sigma_b, mu_b)

            precision_p, sigma_p = self.q_precision_p(sigma_Tn, sigma_b, D_mu_Tn_mu_b)

            precision_p_sigma_b = torch.einsum('pkl,plj->pkj', precision_p, sigma_b)  # P x K x K
            precision_p_sigma_Tn = torch.einsum('pkl,nlj->npkj', precision_p, sigma_Tn)  # N x P x K x K
            traces_precision_p_sigma_b_sigma_Tn = torch.einsum('pkk', precision_p_sigma_b) + torch.einsum('npkk', precision_p_sigma_Tn)  # N x P

            if self.spatial_prior:
                lb = self.lower_bound_with_prior(precision_p, precision_T, mu_Tn, mu_b, mu_w, sigma_Tn, sigma_b,
                                                 sigma_w, beta, alpha, bf_bf, bf_mu_w, D_mu_Tn_mu_b,
                                                 traces_precision_p_sigma_b_sigma_Tn)
            else:
                lb = self.lower_bound_without_prior(precision_p, mu_b, sigma_Tn, sigma_b, beta,
                                                    D_mu_Tn_mu_b, traces_precision_p_sigma_b_sigma_Tn)

            if i > 1:
                # We check that the lower bounds is increasing
                assert lower_bound_list[-1] < lb

            lower_bound_list.append(lb)

            if i > 1:
                diff = lower_bound_list[-1] - lower_bound_list[-2]
                if torch.abs(diff / lower_bound_list[-2]) < self.tol:
                    i = self.n_iter
                    converged = 1

            i += 1

        # Check convergence
        if not converged and self.n_iter > 0:
            print("Algorithm did not converged")
            self.converged = False
        else:
            print("Algorithm converged")
            self.converged = True
        print()

        self.lower_bound_list = [float(i.cpu()) for i in lower_bound_list]
        self.mu_Tn = mu_Tn.cpu().numpy()
        self.sigma_Tn = sigma_Tn.cpu().numpy()
        self.mu_b = mu_b.cpu().numpy()
        self.sigma_b = sigma_b.cpu().numpy()
        self.precision_p = precision_p.cpu().numpy()
        self.sigma_p = sigma_p.cpu().numpy()
        self.beta = float(beta.cpu())

        # Compute metric for rater's performance assessment
        precision_p_sum = np.linalg.inv(np.sum(self.precision_p, axis=0))  # K x K
        self.lambda_p = self.P * np.einsum('kl,plj->pkj', precision_p_sum, self.precision_p)  # P x K x K

        if self.spatial_prior:
            self.mu_w = mu_w.cpu().numpy()
            self.sigma_w = sigma_w.cpu().numpy()
            self.alpha = alpha.cpu().numpy()
            self.precision_T = precision_T.cpu().numpy()
            self.sigma_T = 1 / (precision_T + self.eps).cpu().numpy()
            self.bf_mu_w = bf_mu_w.cpu().numpy() # N x K
            self.basis_functions = basis_functions_cpu

        self.fitted = True




