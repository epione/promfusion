#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from scipy.ndimage.morphology import distance_transform_edt
from scipy.special import logit, expit
from promfusion.Gaussian_FPM_GLSP import Gaussian_FPM_GLSP
from promfusion.Student_FPM_GLSP import Student_FPM_GLSP


def lambda_expit(x, gamma):
    '''
    Sigmoid function used to transform binary maps into probability maps
    '''
    return 1 / (1 + np.exp(-gamma * x))

class FusionProbabilityMaps():

    def __init__(self, config):

        self.config = config
        self.dim = config["dim"]
        self.model = {"Gaussian": Gaussian_FPM_GLSP,
                      "Student": Student_FPM_GLSP}[config["model"]](config)

    def euclidean_distance_transform(self, raters_masks, spacing):

        output = []

        for i in range(raters_masks.shape[0]):
            mask = raters_masks[i]
            dm1 = distance_transform_edt(mask, sampling=spacing)
            dm2 = distance_transform_edt(np.abs(mask - 1), sampling=spacing)
            dm = dm1 - dm2
            output.append(dm)

        return np.stack(output)  # P x img_shape

    def create_narrow_band(self, D):
        '''
        :param D: P x img_shape
        :return: narrow_band
        '''

        union1 = np.logical_and.reduce(D.astype(bool)) # Foreground in common
        union2 = np.logical_and.reduce(np.invert(D.astype(bool))) # Background in common
        union = np.logical_or(union1, union2)

        dm = distance_transform_edt(union) / max(5, self.config["nb_margin"])
        dm[dm >= 1] = 1
        dm[dm < 1] = 0
        narrow_band = np.invert(dm.astype(bool)) # img_shape

        return narrow_band

    def crop(self, img, rater_masks):
        '''
        :param img: img_shape
        :param: raters_masks: P x img_shape
        :return: raters masks cropped
        '''

        rater_masks_bin = np.zeros(rater_masks.shape)
        rater_masks_bin[rater_masks >= 0.5] = 1

        margin = max(3, self.config["crop_margin"])
        output = []

        if self.dim == 3:
            zmax, zmin, imax, imin, jmax, jmin = -np.PINF, np.PINF, -np.PINF, np.PINF, -np.PINF, np.PINF
            for i in range(len(rater_masks)):
                zz, ii, jj = np.where(rater_masks_bin[i])
                zmax, imax, jmax = max(zmax, max(zz)), max(imax, max(ii)), max(jmax, max(jj))
                zmin, imin, jmin = min(zmin, min(zz)), min(imin, min(ii)), min(jmin, min(jj))

            for i in range(len(rater_masks)):
                crop = rater_masks[i]
                crop = crop[max(zmin - margin, 0):min(crop.shape[0], zmax + margin),
                       max(imin - margin, 0):min(crop.shape[1], imax + margin),
                       max(jmin - margin, 0):min(crop.shape[2], jmax + margin)]
                output.append(crop)
            img = img[max(zmin - margin, 0):min(img.shape[0], zmax + margin),
                      max(imin - margin, 0):min(img.shape[1], imax + margin),
                      max(jmin - margin, 0):min(img.shape[2], jmax + margin)]

        elif self.dim == 2:
            imax, imin, jmax, jmin = -np.PINF, np.PINF, -np.PINF, np.PINF
            for i in range(len(rater_masks)):
                ii, jj = np.where(rater_masks_bin[i])
                imax, jmax = max(imax, max(ii)), max(jmax, max(jj))
                imin, jmin = min(imin, min(ii)), min(jmin, min(jj))

            for i in range(len(rater_masks)):
                crop = rater_masks[i]
                crop = crop[max(imin - margin, 0):min(crop.shape[0], imax + margin),
                       max(jmin - margin, 0):min(crop.shape[1], jmax + margin)]
                output.append(crop)
            img = img[max(imin - margin, 0):min(img.shape[0], imax + margin),
                      max(jmin - margin, 0):min(img.shape[1], jmax + margin)]

        else:
            raise ValueError("Dimension not recognized")

        rater_masks = np.stack(output)

        return img, rater_masks

    def find_consensus(self, img, raters_masks, spacing):
        '''
        :param img: img_shape
        :param rater_masks: P x img_shape
        :param spacing: tuple
        '''

        raters_masks = np.copy(raters_masks)
        img = np.copy(img)

        if len(img.shape) != self.dim:
            raise ValueError("The dimension of the image is not the same as the one in the configuration file")

        # Check the inputs
        if self.config["distance"] == "precomputed":
            vmin, vmax = np.amin(raters_masks), np.amax(raters_masks)
            if vmin < 0 or vmax > 1:
                raise ValueError("When distance=precomputed, the raters' masks are assumed to be probabilities")
        else:
            if not ((raters_masks == 0) | (raters_masks == 1)).all():
                raise ValueError("When distance=euclidean, the raters' masks are assumed to be binary with 0"
                                 " for background and 1 for the segmented structure")

        self.img_shape = img.shape

        # First we crop the image around the structure of interest
        if self.config["crop"]:
            img, raters_masks = self.crop(img, raters_masks)
            self.img_shape = img.shape

        # For large images, finding the consensus can be time consuming. We can save time by finind a consensus
        # only for the regions where the raters disagree. The consensus for the rest of the image will be
        # approximated by taking the mean among the raters.
        narrow_band = np.ones(self.img_shape, dtype=np.bool)
        if self.config["use_narrow_band"]:
            if self.config["distance"] == "precomputed":
                raters_masks_bin = np.zeros(raters_masks.shape)
                raters_masks_bin[raters_masks >= 0.5] = 1
            else:
                raters_masks_bin = raters_masks
            narrow_band = self.create_narrow_band(raters_masks_bin)  # img_shape

        # Case of binary inputs. A distance map and the sigmoid function are used to project
        #  the input into the probability space.
        if self.config["distance"] == "euclidean":

            # We create a distance map using an Euclidean distance transform
            distance_raters_masks = self.euclidean_distance_transform(raters_masks, spacing)

            # We build the D matrix
            D = []
            for p in range(distance_raters_masks.shape[0]):

                mask = distance_raters_masks[p]
                mask = lambda_expit(mask, self.config["lambda_expit"])
                raters_masks[p] = mask
                mask = mask[narrow_band]
                dp = np.concatenate([np.reshape(mask, (-1, 1)),
                                     np.reshape(np.abs(1 - mask), (-1, 1))], axis=1)

                D.append(dp)
            D = np.stack(D)  # P x N x K
            D = np.transpose(D, (1, 0, 2))  # N x P x K

        else:
            # raters_masks are already probability masks

            # We build the D matrix
            D = []
            for p in range(raters_masks.shape[0]):
                mask = raters_masks[p]
                mask = mask[narrow_band]
                dp = np.concatenate([np.reshape(mask, (-1, 1)),
                                     np.reshape(np.abs(1 - mask), (-1, 1))], axis=1)
                D.append(dp)
            D = np.stack(D)  # P x N x K
            D = np.transpose(D, (1, 0, 2))  # N x P x K

        # Projection from the probability space to the Euclidean space, where the consensus is found
        if self.config["link_function"] == "logit":
            # In order to avoid infinite numbers
            D = np.clip(D, 1e-7, 1-1e-7)
            D = logit(D)
        elif self.config["link_function"] == "sqrt":
            D = np.sqrt(D)
        else:
            raise ValueError("Link function not recognized. Possibilities are logit or sqrt.")

        # Then we find the consensus
        self.model.fit(D, narrow_band)

        # We transform the consensus back to the probability space
        mu_Tn = self.model.mu_Tn  # N x K
        consensus = np.zeros((mu_Tn.shape[1],) + img.shape) # K x img_shape
        consensus[:, narrow_band] = mu_Tn.T
        if self.config["link_function"] == "sqrt":
            consensus = consensus**2
            consensus = consensus / (np.sum(consensus, axis=0) + 1e-8)
        elif self.config["link_function"] == "logit":
            consensus = expit(consensus)
            consensus = consensus / (np.sum(consensus, axis=0) + 1e-8)
        else:
            raise ValueError("Link function not recognized. Possibilities are logit or sqrt.")

        # We take into account the narrow band. Outside of the narrow band, we take the mean of the raters maps.
        mean_raters_masks = np.mean(raters_masks, axis=0)
        consensus[0][np.invert(narrow_band)] = mean_raters_masks[np.invert(narrow_band)]
        consensus[1][np.invert(narrow_band)] = np.abs(1-mean_raters_masks)[np.invert(narrow_band)]

        # Results
        self.consensus = consensus
        self.img = img
        self.raters_masks = raters_masks
        if self.config["use_narrow_band"]:
            self.narrow_band = narrow_band
